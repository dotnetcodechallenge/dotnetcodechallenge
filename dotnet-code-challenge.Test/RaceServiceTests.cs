﻿using System.Collections.Generic;
using dotnet_code_challenge.FeedData;
using dotnet_code_challenge.Services.DataTransformer;
using dotnet_code_challenge.Services.DataTransformer.Types;
using dotnet_code_challenge.Services.RaceService;
using NUnit.Framework;

namespace dotnet_code_challenge.Test
{
    [TestFixture]
    public class RaceServiceTests
    {
        [Test]
        public void Given_TwoRaces_When_RetrievingRaces_Then_ReturnTwoRaces()
        {
            // Arrange
            var raceService = new RaceService(new FeedDataReader(), new List<IDataTransformer>{new JsonTransformer(), new XmlTransformer()});

            // Act
            var result = raceService.GetRaces().Result;

            // Assert
            Assert.IsTrue(result.Count == 2);
            Assert.AreEqual(result[0].Name, "Evergreen Turf Plate");
            Assert.AreEqual(result[0].Horses[0].Name, "Advancing");
            Assert.AreEqual(result[0].Horses[0].Price, 4.2);
            Assert.AreEqual(result[0].Horses[1].Name, "Coronel");
            Assert.AreEqual(result[0].Horses[1].Price, 12);

            Assert.AreEqual(result[1].Name, "13:45 @ Wolverhampton");
            Assert.AreEqual(result[1].Horses[0].Name, "Toolatetodelegate");
            Assert.AreEqual(result[1].Horses[0].Price, 10);
            Assert.AreEqual(result[1].Horses[1].Name, "Fikhaar");
            Assert.AreEqual(result[1].Horses[1].Price, 4.4);
        }
    }
}
