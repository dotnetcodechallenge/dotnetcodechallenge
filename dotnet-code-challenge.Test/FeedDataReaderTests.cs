using System;
using dotnet_code_challenge.FeedData;
using NUnit.Framework;

namespace dotnet_code_challenge.Test
{
    [TestFixture]
    public class FeedDataReaderTests
    {
        [Test]
        public void Given_TwoFilesExists_When_ReadingFiles_Then_ReturnTwoFiles()
        {
            // Arrange
            var feedDataReader = new FeedDataReader();

            // Act
            var result = feedDataReader.ReadFileData().Result;

            // Assert
            Assert.IsTrue(result.Count == 2);
            Assert.IsFalse(string.IsNullOrEmpty(result[0]));
            Assert.IsFalse(string.IsNullOrEmpty(result[1]));
        }
    }
}
