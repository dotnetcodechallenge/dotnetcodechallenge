﻿namespace dotnet_code_challenge.Models.Internal
{
    public class HorseModel
    {
        public string Name { get; set; }
        public double Price { get; set; }
    }
}
