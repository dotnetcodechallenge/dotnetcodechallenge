﻿using System.Collections.Generic;

namespace dotnet_code_challenge.Models.Internal
{
    public class RaceModel
    {
        public string Name { get; set; }
        public List<HorseModel> Horses { get; set; }
    }
}
