﻿using dotnet_code_challenge.Models.Internal;

namespace dotnet_code_challenge.Services.DataTransformer
{
    public interface IDataTransformer
    {
        bool IsType(string data);
        RaceModel TransformData(string data);
    }
}
