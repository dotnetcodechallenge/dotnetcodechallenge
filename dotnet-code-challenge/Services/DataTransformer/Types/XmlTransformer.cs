﻿using System;
using dotnet_code_challenge.Models.Internal;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace dotnet_code_challenge.Services.DataTransformer.Types
{
    public class XmlTransformer : IDataTransformer
    {
        public bool IsType(string data)
        {
            try
            {
                XDocument.Parse(data);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public RaceModel TransformData(string data)
        {
            FeedData.Models.RaceXml.Meeting dto; 

            using (TextReader reader = new StringReader(data))
            {
                dto = (dotnet_code_challenge.FeedData.Models.RaceXml.Meeting)new XmlSerializer(typeof(dotnet_code_challenge.FeedData.Models.RaceXml.Meeting)).Deserialize(reader);
            }

            var horsePrices = dto.Races.Race.Prices.Price.Horses.Horse;

            var result = new RaceModel
            {
                Name = dto.Races.Race.Name,
                Horses = dto.Races.Race.Horses.Horse.Select(horse => new HorseModel
                {
                    Name = horse.Name,
                    Price = Convert.ToDouble(horsePrices.FirstOrDefault(prices => prices._Number == horse.Number)?.Price)
                }).ToList()
            };

            return result;
        }
    }
}
