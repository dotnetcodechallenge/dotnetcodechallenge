﻿using dotnet_code_challenge.Models.Internal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace dotnet_code_challenge.Services.DataTransformer.Types
{
    public class JsonTransformer : IDataTransformer
    {
        public bool IsType(string data)
        {
            try
            {
                JToken.Parse(data);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public RaceModel TransformData(string data)
        {
            var dto = JsonConvert.DeserializeObject<FeedData.Models.RaceJson.RootObject>(data);

            var result = new RaceModel
            {
                Name = dto.RawData.FixtureName,
                Horses = dto.RawData.Markets.FirstOrDefault()?.Selections.Select(horse => new HorseModel
                {
                    Name = horse.Tags.name,
                    Price = horse.Price
                } ).ToList()
            };

            return result;
        }
    }
}
