﻿using System.Collections.Generic;
using System.Threading.Tasks;
using dotnet_code_challenge.Models.Internal;

namespace dotnet_code_challenge.Services.RaceService
{
    public interface IRaceService
    {
        Task<List<RaceModel>> GetRaces();
    }
}