﻿using dotnet_code_challenge.FeedData;
using dotnet_code_challenge.Models.Internal;
using dotnet_code_challenge.Services.DataTransformer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_code_challenge.Services.RaceService
{
    public class RaceService : IRaceService
    {
        private readonly IFeedDataReader _dataReader;
        private readonly IEnumerable<IDataTransformer> _dataTransformers;

        public RaceService(IFeedDataReader dataReader, IEnumerable<IDataTransformer> dataTransformers)
        {
            _dataReader = dataReader;
            _dataTransformers = dataTransformers;
        }

        public async Task<List<RaceModel>> GetRaces()
        {
            var result = new List<RaceModel>();

            var files = await _dataReader.ReadFileData();

            if (files == null || !files.Any()) return result;

            foreach (var file in files)
            {
                try
                {
                    var race = _dataTransformers.FirstOrDefault(x => x.IsType(file))?.TransformData(file);

                    if (race != null)
                    {
                        result.Add(race);
                    }
                }
                catch (Exception)
                {
                   // Failed
                }
            }

            return result;
        }
    }
}
