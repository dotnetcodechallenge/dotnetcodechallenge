﻿using dotnet_code_challenge.Services.RaceService;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace dotnet_code_challenge
{
    public class Application
    {
        private readonly IRaceService _raceService;

        public Application(IRaceService raceService)
        {
            _raceService = raceService;
        }

        public async Task Run()
        {
            var races = await _raceService.GetRaces();

            foreach (var race in races)
            {
                var horses = race.Horses.OrderBy(x => x.Price);

                Console.WriteLine();
                Console.WriteLine($"Race: {race.Name}");

                foreach (var horse in horses)
                {
                    Console.WriteLine($"Horse: {horse.Name}, Price: {horse.Price}");
                }
            }
        }
    }
}
