﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace dotnet_code_challenge.FeedData
{
    public interface IFeedDataReader
    {
        Task<List<string>> ReadFileData();
    }
}