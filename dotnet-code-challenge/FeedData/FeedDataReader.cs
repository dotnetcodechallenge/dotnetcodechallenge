﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace dotnet_code_challenge.FeedData
{
    public class FeedDataReader : IFeedDataReader
    {
        public async Task<List<string>> ReadFileData()
        {
            const string dataSourcePath = @"FeedData/Files";

            var results = new List<string>();

            var files = Directory.EnumerateFiles(dataSourcePath);

            foreach (var file in files)
            {
                using (var reader = File.OpenText(file))
                {
                    try
                    {
                        results.Add(await reader.ReadToEndAsync());
                    }
                    catch (Exception)
                    {
                        //Console.WriteLine($"Error Parsing Files: Exception {e}");
                    }
                }
            }
            return results;
        }
    }
}
