﻿using System;
using System.Threading.Tasks;
using dotnet_code_challenge.FeedData;
using dotnet_code_challenge.Services.DataTransformer;
using dotnet_code_challenge.Services.DataTransformer.Types;
using dotnet_code_challenge.Services.RaceService;
using Microsoft.Extensions.DependencyInjection;

namespace dotnet_code_challenge
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = SetUpIoC();

            var application = container.GetService<Application>();

            Task.Run(async () => { await application.Run(); }); 

            Console.ReadLine();
        }

        public static IServiceProvider SetUpIoC()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddTransient<Application>();
            serviceCollection.AddTransient<IRaceService, RaceService>();
            serviceCollection.AddTransient<IFeedDataReader, FeedDataReader>();
            serviceCollection.AddTransient<IDataTransformer, JsonTransformer>();
            serviceCollection.AddTransient<IDataTransformer, XmlTransformer>();
            
            var container = serviceCollection.BuildServiceProvider();

            return container;
        }
    }
}
