High Level Overview

# Program

- Set Up IoC Container to manage dependencies
- Retrieve Application from container, Run Application

# Application

- uses IRaceService to retrieve all the Races
- Displays each race and the runners in ascending order

# IRaceService

- uses IFeedDataReader to retrieve the raw data from the files
- for each file, finds the correct IDataTransformer (XML or JSON implementations), and returns the concrete RaceModel
- returns a list of RaceModels

# IFeedDataReader

- Reads all files located at path "FeedData/Files" into a list of strings

# IDataTransformer (JsonTransformer and XmlTransformer implementations)

- checks the type
- If the data matches the type, then it performs the necessary transformation
